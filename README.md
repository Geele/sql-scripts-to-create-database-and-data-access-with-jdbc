# SQL scripts to create database and Data access with JDBC



## Getting started


- Clone the repository to your local machine.

- Make sure you have the necessary dependencies installed, including Postgres and the Postgres SQL driver.

- Update the application.properties file with the correct database URL, username, and password.

- Run the application using the command ./mvnw spring-boot:run or ./gradlew bootRun.
- Test the application by running the methods in the CustomerRepositoryImpl class.



## Description
This project is a Spring Boot application that implements a repository to access data from a Chinook database using plain Java and SQL scripts. The repository provides methods to find all customers, find a customer by id, and insert a customer into the database and also updating customers on the database....


