package com.example.jdbcassignment.Repository;

import com.example.jdbcassignment.Models.Customer;
import com.example.jdbcassignment.Models.CustomerSpender;
import com.example.jdbcassignment.Repository.Interface.CustomerRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerRepositoryImpl implements CustomerRepository {
    private final String url;
    private final String username;
    private final String password;


    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password)
    {
        this.url = url;
        this.username = username;
        this.password = password;
    }

   /* public void test() {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            System.out.println("connected to postgres");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public List<Customer> findAll(){

        String sql = "SELECT * FROM Customer";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url,username,password))  {

            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();

            while (result.next()){
                Customer customer = new Customer(
                        result.getInt("customer_iD"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("company"),
                        result.getString("address"),
                        result.getString("city"),
                        result.getString("state"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("fax"),
                        result.getString("email"),
                        result.getInt("support_rep_id")
                );
                customers.add(customer);

            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return customers;

    }

    @Override
    public Customer findById(Integer id){
        String sql ="SELECT * FROM Customer WHERE customer_id = ?";
        Customer customer = null;

        try (Connection conn = DriverManager.getConnection(url,username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);

            ResultSet result = statement.executeQuery();
            result.next();

            customer = new Customer(
                    result.getInt(1),
                    result.getString(2),
                    result.getString(3),
                    result.getString(4),
                    result.getString(5),
                    result.getString(6),
                    result.getString(7),
                    result.getString(8),
                    result.getString(9),
                    result.getString(10),
                    result.getString(11),
                    result.getString(12),
                    result.getInt(13)
            );

        }catch (SQLException e){
            e.printStackTrace();
        }
        return customer;

    };

    @Override
    public void insert(Customer customer) {
        String sql = "INSERT INTO Customer (customer_iD, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, support_rep_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection conn = DriverManager.getConnection(url,username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.customer_id());
            statement.setString(2, customer.first_name());
            statement.setString(3, customer.last_name());
            statement.setString(4, customer.company());
            statement.setString(5, customer.address());
            statement.setString(6, customer.city());
            statement.setString(7, customer.state());
            statement.setString(8, customer.country());
            statement.setString(9, customer.postal_code());
            statement.setString(10, customer.phone());
            statement.setString(11, customer.fax());
            statement.setString(12, customer.email());
            statement.setInt(13, customer.support_rep_id());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    public List<Customer> findByName(String first_name) {
        String sql = "SELECT * FROM Customer WHERE first_name = ?";
        List<Customer> customers = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, first_name);

            ResultSet result = statement.executeQuery();
            if(!result.next()){
                System.out.println("No customers found with the name "+first_name);
                return customers;
            }
            do{
                Customer customer = new Customer(
                        result.getInt(1),
                        result.getString(2),
                        result.getString(3),
                        result.getString(4),
                        result.getString(5),
                        result.getString(6),
                        result.getString(7),
                        result.getString(8),
                        result.getString(9),
                        result.getString(10),
                        result.getString(11),
                        result.getString(12),
                        result.getInt(13)
                );
                customers.add(customer);
            }while (result.next());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }





    @Override
    public List<Customer> getPage(int limit, int offset){
        String sql ="SELECT * FROM Customer LIMIT ? OFFSET ?";
        List<Customer> customers = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(url,username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);

            ResultSet result = statement.executeQuery();

            while (result.next()) {
                customers.add(new Customer(
                        result.getInt(1),
                        result.getString(2),
                        result.getString(3),
                        result.getString(4),
                        result.getString(5),
                        result.getString(6),
                        result.getString(7),
                        result.getString(8),
                        result.getString(9),
                        result.getString(10),
                        result.getString(11),
                        result.getString(12),
                        result.getInt(13)
                ));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return customers;
    }

    @Override
    public void updateCustomer(Customer customer) {
        String sql = "UPDATE Customer SET first_name = ?, last_name = ?, company = ?, address = ?, city = ?, state = ?, country = ?, postal_code = ?, phone = ?, fax = ?, email = ?, support_rep_id = ? WHERE customer_iD = ?";
        try (Connection conn = DriverManager.getConnection(url,username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(13, customer.customer_id());
            statement.setString(1, customer.first_name());
            statement.setString(2, customer.last_name());
            statement.setString(3, customer.company());
            statement.setString(4, customer.address());
            statement.setString(5, customer.city());
            statement.setString(6, customer.state());
            statement.setString(7, customer.country());
            statement.setString(8, customer.postal_code());
            statement.setString(9, customer.phone());
            statement.setString(10, customer.fax());
            statement.setString(11, customer.email());
            statement.setInt(12, customer.support_rep_id());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



    @Override
    public String getCountryWithMostCustomers() {
        String sql = "SELECT country, COUNT(*) as count FROM Customer GROUP BY country ORDER BY count DESC LIMIT 1";
        String country = null;
        int count = 0;
        try (Connection conn = DriverManager.getConnection(url,username,password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                country = result.getString("country");
                count = result.getInt("count");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return country+" "+count;
    }





    public CustomerSpender getCustomerSpender(String sql) {
        // Initialize customerSpender to null
        CustomerSpender customerSpender = null;
        try {
            // Connect to the database using the provided url, username and password
            Connection conn = DriverManager.getConnection(url, username, password);
            // Prepare the sql statement
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            // Execute the query and get the result set
            ResultSet resultSet = preparedStatement.executeQuery();
            // Check if the result set has any data
            if (resultSet.next()) {
                // Create a new CustomerSpender object and set its properties
                // using the data from the result set
                customerSpender = new CustomerSpender(
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getDouble("sum")
                        );
            }
        } catch (SQLException e) {
            // Handle the exception
            System.err.println("Error while getting customer spender: " + e.getMessage());
        }
        return customerSpender;
    }

    @Override
    public CustomerSpender highestSpender() {
        // SQL query to get the highest spender
        String sql = "SELECT customer.first_name, last_name, SUM(total) FROM invoice INNER JOIN customer ON customer.customer_id = invoice.customer_id GROUP BY customer.customer_id ORDER BY SUM(total) DESC LIMIT 1";
        // Call the helper method to get the highest spender
        return getCustomerSpender(sql);
    }





    public List<String> getMostPopularGenre(int customerId) {
        List<String> mostPopularGenres = new ArrayList<>();
        try {
            // Connect to the database using the provided url, username and password
            Connection conn = DriverManager.getConnection(url, username, password);
            // SQL query to get the most popular genre for a given customer
            String sql = "SELECT genre.name, COUNT(track.genre_id) as count FROM invoice_line " +
                    "INNER JOIN track ON invoice_line.track_id = track.track_id " +
                    "INNER JOIN genre ON track.genre_id = genre.genre_id " +
                    "WHERE invoice_line.invoice_id IN (SELECT invoice_id FROM invoice WHERE customer_id = ?) " +
                    "GROUP BY genre.name " +
                    "ORDER BY count DESC ";
            // Prepare the sql statement
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, customerId);
            // Execute the query and get the result set
            ResultSet resultSet = preparedStatement.executeQuery();
            int maxCount = 0;
            // Iterate over the result set
            while (resultSet.next()) {
                int count = resultSet.getInt("count");
                if (count > maxCount) {
                    maxCount = count;
                    mostPopularGenres.clear();
                    mostPopularGenres.add(resultSet.getString("name"));
                } else if (count == maxCount) {
                    mostPopularGenres.add(resultSet.getString("name"));
                }
            }
        } catch (SQLException e) {
            // Handle the exception
            System.err.println("Error while getting most popular genre: " + e.getMessage());
        }
        return mostPopularGenres;
    }






}
