package com.example.jdbcassignment.Repository.Interface;

import java.util.List;

public interface CRUDRepository <T,U> {
    List <T> findAll();
    T findById(U id);
     void insert(T object);
    List getPage(int limit, int offset);
}
