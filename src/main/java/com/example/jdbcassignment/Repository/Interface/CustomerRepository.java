package com.example.jdbcassignment.Repository.Interface;

import com.example.jdbcassignment.CustomerNotFoundException;
import com.example.jdbcassignment.Models.Customer;
import com.example.jdbcassignment.Models.CustomerSpender;

import java.sql.SQLException;
import java.util.List;

public interface CustomerRepository extends CRUDRepository <Customer, Integer> {
    List<Customer>  findByName(String name);

    void updateCustomer(Customer customer) throws CustomerNotFoundException;

    String getCountryWithMostCustomers();
   // String getCustomerSpender();

    CustomerSpender highestSpender() throws SQLException;

    List<String> getMostPopularGenre(int customer_id);
}
