package com.example.jdbcassignment.Runner;

import com.example.jdbcassignment.Models.Customer;
import com.example.jdbcassignment.Repository.Interface.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class JDBCAppRunner implements ApplicationRunner {
    CustomerRepository customerRepository;

    public JDBCAppRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("app runner runs");
       Customer customer = new Customer(60,"Geele","Egal","Experis","Oslo gata","Oslo"," Østlandet","Norway","0645","*********","*******","Myemail@google.com",3);
       //  customerRepository.insert(customer);
      //  System.out.println(customerRepository.highestSpender());
      //  System.out.println(customerRepository.getCountryWithMostCustomers());
         System.out.println(customerRepository.findByName("Geele"));
      //  System.out.println(customerRepository.getPage(1,59));
       // System.out.println(customerRepository.findById(60));
      //  System.out.println(customerRepository.getMostPopularGenre(1));
      //  System.out.println(customerRepository.findAll());
      //  customerRepository.updateCustomer(customer);
        customerRepository.findByName("Geele");

    }

}
