package com.example.jdbcassignment;

public class CustomerNotFoundException extends Exception{

    public CustomerNotFoundException(String message) {
        super("The Customer with the provided 'ID' is not found ");
    }


}
