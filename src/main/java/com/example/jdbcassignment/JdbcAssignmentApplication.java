package com.example.jdbcassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdbcAssignmentApplication {
	public static void main(String[] args) {
		SpringApplication.run(JdbcAssignmentApplication.class, args);
	}
}
