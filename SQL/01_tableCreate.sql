Drop TABLE IF EXISTS assistant;
Drop TABLE IF EXISTS powers;
Drop TABLE IF EXISTS superhero;


-- Create the superhero table
CREATE TABLE superhero(
	heroId SERIAL PRIMARY KEY,
	heroName TEXT,
	heroAlias TEXT,
	orgin TEXT
);

-- Create the assistant table
CREATE TABLE assistant(
	assistantId SERIAL PRIMARY KEY,
	assistantName TEXT
);

-- Create the powers table
CREATE TABLE powers(
	powersId SERIAL PRIMARY KEY,
	powersName TEXT,
	powersDescription TEXT
);


