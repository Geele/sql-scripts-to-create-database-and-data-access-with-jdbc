
-- Add a column to the assistant table to store the ID of the superhero.
ALTER TABLE assistant ADD COLUMN heroId INTEGER;

/*Add a foreign key constraint to the assistant table that
references the id column in the superhero table*/
ALTER TABLE assistant ADD CONSTRAINT fk_assistantheroId
FOREIGN KEY (heroId) REFERENCES Superhero (heroId);