DROP TABLE IF EXISTS SuperheroPower;

-- Create the superhero_powers join table
CREATE TABLE SuperheroPower (
  heroId INTEGER REFERENCES Superhero (heroId),
  powersId INTEGER REFERENCES powers (powersId),
  PRIMARY KEY (heroId, powersId)
);