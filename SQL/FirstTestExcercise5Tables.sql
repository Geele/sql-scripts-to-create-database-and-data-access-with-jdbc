DROP TABLE IF EXISTS student_subject;

DROP TABLE IF EXISTS research_project;
DROP TABLE IF EXISTS student;

DROP TABLE IF EXISTS subject;

DROP TABLE IF EXISTS professor;

CREATE TABLE professor (
	prof_id int PRIMARY KEY,
	prof_name varchar(50) NOT NULL,
	field varchar(50)
	
);

CREATE TABLE research_project (
	proj_id serial PRIMARY KEY,
	title varchar(100) NOT NULL
);

CREATE TABLE student (
	stud_id int PRIMARY KEY,
	stud_name varchar(50) NOT NULL,
	project_id int NOT NULL REFERENCES research_project,
	supervisor_id int REFERENCES professor
);

CREATE TABLE subject (
	sub_id int PRIMARY KEY,
	sub_code varchar(10) NOT NULL,
	sub_title varchar(100) NOT NULL,
	lecturer_id int NOT NULL REFERENCES professor
);

CREATE TABLE student_subject (
	stud_id int REFERENCES student,
	sub_id int REFERENCES subject,
	PRIMARY KEY (stud_id, sub_id)
);

INSERT INTO professor VALUES (1, 'Ola Nordmann', 'AI');
INSERT INTO professor(prof_id, prof_name, field)
VALUES (2, 'Kari Nordmann', 'Security Governance'),
		(3, 'Kjell Bob', 'NAV FULLSTACK');
SELECT * FROM professor;

ALTER TABLE student DROP COLUMN project_id;
ALTER TABLE research_project ADD COLUMN stud_id int REFERENCES student;

ALTER TABLE professor ADD COLUMN hjemby varchar(30);


INSERT INTO student (stud_id, stud_name, supervisor_id) VALUES (1, 'Bob Smith', 1);
INSERT INTO student VALUES (2, 'Alice Smith', 2);
INSERT INTO student VALUES (3, 'Jens Fred', 2);
INSERT INTO student VALUES (4, 'Al Fred');

INSERT INTO research_project (title, stud_id) VALUES ('Exploring the Applications of Deep Learning to traffic control', 1);
INSERT INTO research_project (title, stud_id) VALUES ('Applications of Security Governance in DevOps', 2);

INSERT INTO subject (sub_id, sub_code, sub_title, lecturer_id) VALUES (1, 'ONT401', 'Advanced programming', 1);
INSERT INTO subject VALUES (2, 'DL101', 'Applications of Deep Learning', 1);
INSERT INTO subject VALUES (3, 'SEC401', 'Security Governance', 2);
INSERT INTO subject VALUES (4, 'DS401', 'Securing applications', 2);

INSERT INTO student_subject (stud_id, sub_id) 
VALUES 
	(1,1),	
	(1,2),	
	(1,4),
	(2,3),
	(2,4);

SELECT stud_name, prof_name
FROM student
RIGHT JOIN professor
ON supervisor_id = prof_id;